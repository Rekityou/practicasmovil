package com.example.pracicasmovil;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.ArrayList;

public class PracticaSpinner extends AppCompatActivity {
    private Spinner sp;
    private Button btnFinalizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_spinner);

        btnFinalizar = findViewById(R.id.btnFinalizar);

        ArrayList<PracticaDatos> list = new ArrayList<>();
        list.add((new PracticaDatos(getString(R.string.itemFrappses), getString(R.string.msgFrappses), R.drawable.categorias)));
        list.add((new PracticaDatos(getString(R.string.itemAgradecimiento), getString(R.string.msgAgradecimiento), R.drawable.agradecimiento)));
        list.add((new PracticaDatos(getString(R.string.itemAmor), getString(R.string.msgAmor), R.drawable.corazon)));
        list.add((new PracticaDatos(getString(R.string.itemNewYear), getString(R.string.msgNewYear), R.drawable.nuevo)));
        list.add((new PracticaDatos(getString(R.string.itemCanciones), getString(R.string.msgCanciones), R.drawable.canciones)));

        sp = findViewById(R.id.spinner1);


        PracticaAdaptador adapter = new PracticaAdaptador(this, R.layout.spinner_layout, R.id.lblCategorias, list);
        sp.setAdapter(adapter);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                Toast.makeText(parent.getContext(),
                        getString(R.string.msgSeleccionado).toString() + " " + ((PracticaDatos) parent.getItemAtPosition(position))
                                .getTextCategoria(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}